# Maas FrontEnd

Este Software es para los Trabajadores de la empresa GuardianesInformáticos SpA. Ellos tiene un negocio que ellos llaman MaaS
(Monitoring as a Service) y su personal son expertos ingenieros devops y programadores, y
su tarea es monitorear servicios de otras empresas.

Con Mass podrán:
*Selecionar una compañia.
*Selecionar una semana que se va monitorear.
*Registrar las disponibilidad de horas por cada empleado.
*Balancear la carga semanal.


## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

Node (at least the latest LTS)
Yarn (at least 1.0)

```
git clone https://saulemprendedor@bitbucket.org/saulemprendedor/maas-frontend.git
```

### Instalación 🔧


_instalar dependencias_

```
yarn install
# Launch the dev server
yarn dev
```
navegar la app:
  App running at:
  - Local:   http://localhost:8080/
  - Network: http://192.168.0.3:8080/


## Ejecutando las pruebas ⚙️

_Hay 2 tipos de pruebas, hice algunas con Jest y otras con cypress_

### Analice las pruebas end-to-end 🔩

_Las pruebas Cypress analice lo siguiente:_
*Listado de compañias
*Listado de Semanas por compañia
*Selecion de de una semana
*Habilitas ediccion al selecionar un Semana y Guardar la ediccion
*Analizar si el proceso de balanceo de asignaciones de horas esta estable.

```
# Launch the dev server with the Cypress client for
# test-driven development in a friendly interface
yarn dev:e2e
```
Al abrir el browser de Cypress -> click en "Run All Specs"

el codigo de la prueba esta en:
tests/e2e/specs/home.e2e.js

### Y las pruebas de estilo de codificación ⌨️

_Esta prueba unitarias las hice en Jest, con el objetivo de testear la integridad de metodo y funciones de javascript del componente principal._

``` 
# Run unit tests
yarn test:unit
```
el codigo de la prueba esta en:
maas-frontend/src/router/views/home.unit.js


## Expresiones de Gratitud 🎁

* Vale Destacar que ninguna de estas herramientas eran de mi conocimiento, lo hice como  un reto porque ustedes trabajan con Vue.js y pruebas de Frontend igual me parece unas herramientas super!!. Me siento orgulloso de este ejercicio porque aprendi muchas cosas en 3 dias, imagino lo que puedo hacer en semanas.

💎🚀
