import Vue from 'vue'
import { BootstrapVue, IconsPlugin, FormSelectPlugin } from 'bootstrap-vue'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import numeral from 'numeral';
import numFormat from 'vue-filter-number-format';
import Home from './home.vue'

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(FormSelectPlugin);
Vue.filter('numFormat', numFormat(numeral));

// create an extended `Vue` constructor
const localVue = createLocalVue()

const companies = [{
  "business_time": {
    "0": [19, 20, 21, 22, 23],
    "1": [19, 20, 21, 22, 23],
    "2": [19, 20, 21, 22, 23],
    "3": [19, 20, 21, 22, 23],
    "4": [19, 20, 21, 22, 23],
    "5": [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
    "6": [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
  },
  "id": 1,
  "name": "turbus.cl",
  "total_hours": 53
}, {
  "business_time": {
    "0": [10, 11, 12, 13, 16, 17, 18, 19, 20],
    "1": [10, 11, 12, 13, 16, 17, 18, 19, 20],
    "2": [10, 11, 12, 13, 16, 17, 18, 19, 20],
    "3": [10, 11, 12, 13, 16, 17, 18, 19, 20],
    "4": [10, 11, 12, 13, 16, 17, 18, 19, 20],
    "5": [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
    "6": [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
  },
  "id": 2,
  "name": "pullman.cl",
  "total_hours": 73
}];

const employees = [{
  "id": 1,
  "company_id": 1,
  "name": "Ernesto",
  "color": "#ECB477",
  "created_at": "2020-11-11T22:53:12.298Z",
  "updated_at": "2020-11-11T22:53:12.298Z"
}, {
  "id": 2,
  "company_id": 1,
  "name": "Bárbara",
  "color": "#B87F9F",
  "created_at": "2020-11-11T22:53:12.298Z",
  "updated_at": "2020-11-11T22:53:12.298Z"
}, {
  "id": 3,
  "company_id": 1,
  "name": "Benjamín",
  "color": "#769EE4",
  "created_at": "2020-11-11T22:53:12.298Z",
  "updated_at": "2020-11-11T22:53:12.298Z"
}];

const weeks = [{
  "company_id": 1,
  "employee_availability": {
    "0": {
      "19": {
        "1": false,
        "2": false,
        "3": true
      },
      "20": {
        "1": false,
        "2": false,
        "3": true
      },
      "21": {
        "1": false,
        "2": false,
        "3": true
      },
      "22": {
        "1": false,
        "2": false,
        "3": true
      },
      "23": {
        "1": false,
        "2": false,
        "3": true
      }
    },
    "1": {
      "19": {},
      "20": {},
      "21": {},
      "22": {},
      "23": {}
    },
    "2": {
      "19": {},
      "20": {},
      "21": {},
      "22": {},
      "23": {}
    },
    "3": {
      "19": {},
      "20": {},
      "21": {},
      "22": {},
      "23": {}
    },
    "4": {
      "19": {},
      "20": {},
      "21": {},
      "22": {},
      "23": {}
    },
    "5": {
      "10": {},
      "11": {},
      "12": {},
      "13": {},
      "14": {},
      "15": {},
      "16": {},
      "17": {},
      "18": {},
      "19": {},
      "20": {},
      "21": {},
      "22": {},
      "23": {},
    },
    "6": {
      "10": {},
      "11": {},
      "12": {},
      "13": {},
      "14": {},
      "15": {},
      "16": {},
      "17": {},
      "18": {},
      "19": {},
      "20": {},
      "21": {},
      "22": {},
      "23": {}
    }
  },
  "id": 1,
  "monitoring_assignment": {
    "0": {
      "19": 3,
      "20": 3,
      "21": 3,
      "22": 3,
      "23": 3
    },
    "1": {
      "19": null,
      "20": null,
      "21": null,
      "22": null,
      "23": null
    },
    "2": {
      "19": null,
      "20": null,
      "21": null,
      "22": null,
      "23": null
    },
    "3": {
      "19": null,
      "20": null,
      "21": null,
      "22": null,
      "23": null
    },
    "4": {
      "19": null,
      "20": null,
      "21": null,
      "22": null,
      "23": null
    },
    "5": {
      "10": null,
      "11": null,
      "12": null,
      "13": null,
      "14": null,
      "15": null,
      "16": null,
      "17": null,
      "18": null,
      "19": null,
      "20": null,
      "21": null,
      "22": null,
      "23": null
    },
    "6": {
      "10": null,
      "11": null,
      "12": null,
      "13": null,
      "14": null,
      "15": null,
      "16": null,
      "17": null,
      "18": null,
      "19": null,
      "20": null,
      "21": null,
      "22": null,
      "23": null
    },
  },
  "range": "20200926-20201001",
  "sub_title": "Semana 39 del 2020",
  "title": "del 26/09/2020 al 01/10/2020"
}, {
  "company_id": "1",
  "employee_availability": null,
  "id": null,
  "monitoring_assignment": null,
  "range": "20201102-20201108",
  "sub_title": "Semana 45 del 2020",
  "title": "del 02/11/2020 al 08/11/2020"
}];

const employee_availability = {
  '0': {
    '19': {},
    '20': {},
    '21': {},
    '22': {},
    '23': {}
  },
  '1': {
    '19': {},
    '20': {},
    '21': {},
    '22': {},
    '23': {}
  },
  '2': {
    '19': {},
    '20': {},
    '21': {},
    '22': {},
    '23': {}
  },
  '3': {
    '19': {},
    '20': {},
    '21': {},
    '22': {},
    '23': {}
  },
  '4': {
    '19': {},
    '20': {},
    '21': {},
    '22': {},
    '23': {}
  },
  '5': {
    '10': {},
    '11': {},
    '12': {},
    '13': {},
    '14': {},
    '15': {},
    '16': {},
    '17': {},
    '18': {},
    '19': {},
    '20': {},
    '21': {},
    '22': {},
    '23': {}
  },
  '6': {
    '10': {},
    '11': {},
    '12': {},
    '13': {},
    '14': {},
    '15': {},
    '16': {},
    '17': {},
    '18': {},
    '19': {},
    '20': {},
    '21': {},
    '22': {},
    '23': {}
  },
};

// install plugins as normal
localVue.use(BootstrapVue)

describe('Home', () => {
  test('is a valid view', () => {
    expect(Home).toBeAViewComponent()
  })

  test('utility, ids creator', () => {
    expect('check_0_19_1').toEqual(Home.methods.getIdAttr('check_', '0', '19', '1'));
  });

  test('utility, ids creator without employee', () => {
    expect('check_0_19').toEqual(Home.methods.getIdAttr('check_', '0', '19'));
  });

  test('utility, get employee', () => {
    expect(employees[0]).toEqual(Home.methods.getEmployeeById(1, employees));
  });

  test('validate week with empty', () => {
    const wrapper = shallowMount(Home, {
      localVue
    })
    wrapper.vm.companySelected = companies[0];
    wrapper.vm.weekSelected = weeks[1];
    expect(employee_availability).toEqual(wrapper.vm.validateWeek())
  })
})
