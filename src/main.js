import Vue from 'vue'
import { BootstrapVue, IconsPlugin, FormSelectPlugin } from 'bootstrap-vue'
import numeral from 'numeral';
import numFormat from 'vue-filter-number-format';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import router from '@router'
import store from '@state/store'
import App from './app.vue'


// Globally register all `_base`-prefixed components
import '@components/_globals'

// Don't warn about using the dev version of Vue in development.
Vue.config.productionTip = process.env.NODE_ENV === 'production'
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(FormSelectPlugin);
Vue.use(require('vue-moment'));
Vue.filter('numFormat', numFormat(numeral));
// If running inside Cypress...
if (process.env.VUE_APP_TEST === 'e2e') {
  // Ensure tests fail when Vue emits an error.
  Vue.config.errorHandler = window.Cypress.cy.onUncaughtException
}

const app = new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')

// If running e2e tests...
if (process.env.VUE_APP_TEST === 'e2e') {
  // Attach the app to the window, which can be useful
  // for manually setting state in Cypress commands
  // such as `cy.logIn()`.
  window.__app__ = app
}
