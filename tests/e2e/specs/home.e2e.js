describe('Home Page', () => {
  beforeEach(() => {
  })

  it('looks inside the head content using `cy.document()`', () => {
    cy.visit('/')
    cy.document()
  })

  it('Cantidad de compañia', () => {
    cy.get('#company').find('option').should('have.length', 3)
  })

  it('Cantidad de semanas', () => {
    cy.get('#company').select('turbus.cl')
    cy.wait(500)
    cy.get('#company').find('option').should('have.length', 3)
  })

  it('selecionar ultima opcion sin asignaciones', () => {
    cy.get('#week').select('del 26/09/2020 al 01/10/2020')
    cy.get('#button-editar').click()
  })

  it('Guardar disponibilidad de empleados y evaluar balanceo', () => {
    cy.get('#button-guardar').click()
    cy.get('#result_emp_x_x_1').contains('21')
    cy.get('#result_emp_x_x_2').contains('5')
    cy.get('#result_emp_x_x_3').contains('21')
  })

})
